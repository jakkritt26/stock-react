import React from 'react';
// import {Button,FormControl,input,select,textarea}  from 'react-bootstrap';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = { email: '',password:'' };
  }

  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }

  handleSubmit = (event) => {
    alert('A form was submitted: ' + this.state.password);

    // fetch('https://your-node-server-here.com/api/endpoint', {
    //     method: 'POST',
    //     // We convert the React state to JSON and send it as the POST body
    //     body: JSON.stringify(this.state)
    //   }).then(function(response) {
    //     console.log(response)
    //     return response.json();
    //   });

    event.preventDefault();
}
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <div class="main-panel" style={{height:'100vh',width:'100%'}}>     
      <div class="container h-100">
        <div class="row h-100 justify-content-md-center  align-items-center">
        <div class='col-lg-6 col-md-6 col-12'>
        <div class="card card-stats">
              <div class="card-body ">
                <div class="row ">
                      <div class="col-lg-12 py-3">
                       <h2 class="">Login</h2>
                       <div class="py-2">Email / Username : 
                       <input type="text" class="form-control" value={this.state.value} name="email" onChange={this.handleChange}/></div>
                       <div  class="py-2">Password : <input type="password"  class="form-control" value={this.state.value} name="password" onChange={this.handleChange}/></div>
                       <div  class="py-2"><button type="submit" class="btn btn-success btn-block">Login !!!</button></div>
                      </div>
                  </div>
            </div>
        </div>
    
        </div>
        </div>
      </div>

      </div>
      </form>
    );

  }

  }
export default Login