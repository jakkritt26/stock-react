import React from 'react'


class Home extends React.Component {
  render() {
    return (
       
        <div class="main-panel" style={{height:'100vh'}}>     
         <h3 style={{paddingTop:'1em',paddingLeft:'1em'}}>DASHBOARD</h3>
          <div class="content">          
            <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-globe text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <div class="card-category">Capacity</div>
                      <div class="card-title">150GB<div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr></hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i>
                  Update Now
                </div>
              </div>
            </div>
          </div>
          
      </div></div>

      <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card card-stats">
              <div class="card-body ">
                <div class="row">
                  <div class="col-5 col-md-4">
                    <div class="icon-big text-center icon-warning">
                      <i class="nc-icon nc-globe text-warning"></i>
                    </div>
                  </div>
                  <div class="col-7 col-md-8">
                    <div class="numbers">
                      <div class="card-category">Capacity</div>
                      <div class="card-title">150GB<div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ">
                <hr></hr>
                <div class="stats">
                  <i class="fa fa-refresh"></i>
                  Update Now
                </div>
              </div>
            </div>
          </div>
          
      </div></div>
      
      
      </div></div></div>

    )
  }
}
export default Home