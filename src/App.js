import React from 'react';
import logo from './logo.svg';
import Home from './components/home.js';
import Login from './components/login.js';
import Menu from './components/menu.js';

import { Redirect } from 'react-router';
import './App.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";


class App extends React.Component  {
   url_link ="";
  constructor() {
    super();
    this.state = {
      name: "React",
      isUserAuthenticated: false
    };
    this.url_link = window.location.pathname;
   
   
  }
 
  

  render() {
   
  return (
       
    <Router>
      <body class="">
      <div class="wrapper ">
      {this.url_link=='/login'? null :<Menu/>}
     
          <Switch> <Route
                exact
                path="/"
                render={() => {
                    return (
                      this.state.isUserAuthenticated ?
                      <Redirect to="/home" /> : <Redirect to="/login" />
                    )
                }}
              />
       
        <Route path="/home" component={Home} />
        <Route path="/login" component={Login} />
            </Switch>
            </div></body>
     </Router>
    )
  }

}


export default App;
